#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

public class SpawnerController : MonoBehaviour
{
    private const float spawnForce = 100;
    private const float angleVariation = 30;
    private const float SpawnInterval = 2;

    [SerializeField]
    private AsteroidController asteroid;

    [SerializeField]
    private float radius;

    private Transform myTransform;

    private void Awake() => myTransform = transform;

    private void Start() => InvokeRepeating(nameof(Spawn), 1, SpawnInterval);

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        Handles.color = Color.green;
        Handles.DrawWireDisc(transform.position, Vector3.forward, radius);
    }
#endif

    // ABSTRACTION
    private void Spawn()
    {
        var randomPointOnCircle = Random.insideUnitCircle.normalized;
        var newAsteroid = Instantiate(asteroid, randomPointOnCircle * radius, asteroid.transform.rotation);
        newAsteroid.transform.eulerAngles = new Vector3(0, 0, Random.Range(-angleVariation, angleVariation));
        newAsteroid.GetComponent<Rigidbody2D>().AddForce(newAsteroid.transform.rotation * -randomPointOnCircle * spawnForce);
    }

}
