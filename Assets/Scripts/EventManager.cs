using UnityEngine;

public class EventManager : MonoBehaviour
{
    internal delegate void Collision();

    internal event Collision playerDead;
    internal event Collision asteroidShot;

    internal void TriggerPlayerDead() => playerDead?.Invoke();

    internal void TriggerAsteroidShot() => asteroidShot?.Invoke();

}
