using TMPro;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private const int TimeToResetInSeconds = 2;
    private const int ScoreMultiplier = 100;
    private const int InitialLives = 3;

    [SerializeField]
    private TextMeshProUGUI livesUI;

    [SerializeField]
    private TextMeshProUGUI scoreUI;

    [SerializeField]
    private GameObject endGameUI;

    [SerializeField]
    private PlayerController player;

    [SerializeField]
    private ParticleSystem particles;

    private EventManager eventManager;
    private int lives;
    private int score;
    private float timeOffset;

    private void Awake()
    {
        lives = InitialLives;
        score = 0;
        timeOffset = 0;
        endGameUI.SetActive(false);
        eventManager = FindObjectOfType<EventManager>();
    }

    private void Start()
    {
        livesUI.text = lives.ToString();
        scoreUI.text = score.ToString();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && Time.timeScale == 0)
        {
            RestartGame();
        }
    }

    private void OnEnable()
    {
        eventManager.playerDead += OnPlayerDead;
        eventManager.asteroidShot += OnAsteroidShot;
    }

    private void OnDisable()
    {
        eventManager.playerDead -= OnPlayerDead;
        eventManager.asteroidShot -= OnAsteroidShot;
    }

    // ABSTRACTION
    private void OnPlayerDead()
    {
        if (lives == 0)
        {
            EndGame();
        }
        else
        {
            player.gameObject.SetActive(false);
            livesUI.text = (--lives).ToString();
            Invoke(nameof(ResetPlayer), TimeToResetInSeconds);
        }
    }

    // ABSTRACTION
    private void OnAsteroidShot() => scoreUI.text = (++score * (int)(Time.time - timeOffset) * ScoreMultiplier).ToString();
    
    // ABSTRACTION
    private void EndGame()
    {
        particles.Stop();
        timeOffset = Time.time;
        player.gameObject.SetActive(false);
        endGameUI.SetActive(true);
        Time.timeScale = 0;
    }

    // ABSTRACTION
    private void RestartGame()
    {
        foreach (var asteroid in FindObjectsOfType<AsteroidController>())
        {
            Destroy(asteroid.gameObject);
        }
        foreach (var powerup in FindObjectsOfType<PowerupController>())
        {
            Destroy(powerup.gameObject);
        }
        player.gameObject.SetActive(true);
        endGameUI.SetActive(false);
        scoreUI.text = (score = 0).ToString();
        livesUI.text = (lives = InitialLives).ToString();
        ResetPlayer();
        Time.timeScale = 1;
    }

    // ABSTRACTION
    private void ResetPlayer()
    {
        player.gameObject.SetActive(true);
        player.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        player.transform.position = Vector3.zero;
        player.transform.rotation = Quaternion.identity;
    }
}
