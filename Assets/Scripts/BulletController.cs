using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class BulletController : MonoBehaviour
{
    private const float BulletForceMultiplier = 100;

    private Rigidbody2D myRBody;
    private Transform myTransform;

    private void Awake()
    {
        myTransform = transform;
        myRBody = GetComponent<Rigidbody2D>();
    }

    private void OnEnable() => myRBody.AddForce(myTransform.up.normalized * BulletForceMultiplier);

    private void OnTriggerEnter2D(Collider2D other) => Destroy(gameObject);

    private void OnCollisionEnter2D(Collision2D collision) => Destroy(gameObject);

}
