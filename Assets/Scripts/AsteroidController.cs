using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(SpriteRenderer))]
public class AsteroidController : MonoBehaviour
{
    private const float Torque = 50;
    private const float MinScale = 0.7f;
    private const float MaxScale = 2f;
    private const float SplitForce = 40;
    private const float SplitScaleReduction = 0.5f;
    private const float SplitThreshold = 1.2f;

    [SerializeField]
    private Sprite[] sprites;

    private ParticleSystem particles;
    private EventManager eventManager;
    private Rigidbody2D myRBody;
    private Transform myTransform;
    private SpriteRenderer myRenderer;
    private float scale;

    private void Awake()
    {
        eventManager = FindObjectOfType<EventManager>();
        myTransform = transform;
        myRBody = GetComponent<Rigidbody2D>();
        myRenderer = GetComponent<SpriteRenderer>();
        Invoke(nameof(SelfDestruct), 60);
        particles = FindObjectOfType<ParticleSystem>();
    }

    private void Start()
    {
        myRenderer.sprite = sprites[Random.Range(0, sprites.Length)];
        myRBody.AddTorque(Random.Range(-Torque, Torque));
        scale = scale == 0 ? Random.Range(MinScale, MaxScale) : scale;
        myTransform.localScale *= scale;
        myRBody.mass = scale;
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        switch (LayerMask.LayerToName(collider.gameObject.layer))
        {
            case "Bullet":
                if (myTransform.localScale.x > SplitThreshold)
                {
                    Split(collider.gameObject.transform);
                }
                else
                {
                    SelfDestruct();
                }
                particles.transform.position = myTransform.position;
                particles.Play();
                eventManager.TriggerAsteroidShot();
                break;
        }
    }

    private void SetScale(float scale) => this.scale = scale;
    
    // ABSTRACTION
    private void Split(Transform collision)
    {
        CreateChunk(collision.right);
        CreateChunk(-collision.right);
        SelfDestruct();
    }

    // ABSTRACTION
    private void CreateChunk(Vector3 direction)
    {
        var chunk = Instantiate(this, myTransform.position + direction * scale, myTransform.rotation);
        chunk.SetScale(scale * SplitScaleReduction);
        chunk.GetComponent<Rigidbody2D>().AddForce(direction * SplitForce);
    }

    // ABSTRACTION
    private void SelfDestruct() => Destroy(gameObject);

}
