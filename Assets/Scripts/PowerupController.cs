using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerupController : MonoBehaviour
{
    // ENCAPSULATION
    public Powerup Powerup => powerup;

    [SerializeField]
    private Powerup powerup;

}
