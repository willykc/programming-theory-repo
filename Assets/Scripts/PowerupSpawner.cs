using UnityEngine;

public class PowerupSpawner : MonoBehaviour
{
    [SerializeField]
    private float frequency;

    [SerializeField]
    private float padding;

    [SerializeField]
    private Transform top;

    [SerializeField]
    private Transform bottom;

    [SerializeField]
    private Transform left;

    [SerializeField]
    private Transform right;

    [SerializeField]
    private GameObject[] powerUpPrefabs;

    private void Awake()
    {
        InvokeRepeating(nameof(Spawn), 0, frequency);
    }

    // ABSTRACTION
    private void Spawn()
    {
        var randomx = Random.Range(left.position.x + padding, right.position.x - padding);
        var randomy = Random.Range(bottom.position.y + padding, top.position.y - padding);
        var position = new Vector3(randomx, randomy);
        var prefab = powerUpPrefabs[Random.Range(0, powerUpPrefabs.Length)];
        Instantiate(prefab, position, prefab.transform.rotation);
    }

}
