using UnityEngine;

public abstract class Powerup : ScriptableObject
{
    // ENCAPSULATION
    public float Duration => duration;

    [SerializeField]
    private float duration;

    public abstract void Apply(PlayerController player);

    public abstract void Remove(PlayerController player);
}
