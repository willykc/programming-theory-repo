using UnityEngine;

[CreateAssetMenu(menuName = "Powerups/Speed")]
public class SpeedPowerup : Powerup // INHERITANCE
{
    // POLYMORPHISM
    public override void Apply(PlayerController player)
    {
        player.ForceMultiplier *= 2;
    }
    
    // POLYMORPHISM
    public override void Remove(PlayerController player)
    {
        player.ForceMultiplier *= .5f;
    }

}
