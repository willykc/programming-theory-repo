using UnityEngine;

[CreateAssetMenu(menuName = "Powerups/Shield")]
public class ShieldPowerup : Powerup // INHERITANCE
{
    // POLYMORPHISM
    public override void Apply(PlayerController player)
    {
        player.MakeInvincible();
    }

    // POLYMORPHISM
    public override void Remove(PlayerController player)
    {
        player.MakeVulnerable();
    }
}
