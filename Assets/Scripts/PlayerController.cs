using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerController : MonoBehaviour
{
    private const float TorqueMultiplier = 1;
    private const int InvincibilityTimeInSeconds = 3;

    // ENCAPSULATION
    public float ForceMultiplier
    {
        get
        {
            return playerForceMultiplier;
        }
        set
        {
            playerForceMultiplier = value;
        }
    }

    [SerializeField]
    private BulletController bullet;

    [SerializeField]
    private ParticleSystem particles;

    private Rigidbody2D myRBody;
    private Transform myTransform;
    private EventManager eventManager;
    public float playerForceMultiplier = 4;

    public void MakeInvincible() => gameObject.layer = LayerMask.NameToLayer("Ignore Collisions");

    public void MakeVulnerable() => gameObject.layer = LayerMask.NameToLayer("Player");

    private void Awake()
    {
        myTransform = transform;
        myRBody = GetComponent<Rigidbody2D>();
        eventManager = FindObjectOfType<EventManager>();
    }

    private void OnEnable() => Invoke(nameof(MakeVulnerable), InvincibilityTimeInSeconds);

    private void OnDisable() => MakeInvincible();

    private void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W))
        {
            myRBody.AddForce(myTransform.up.normalized * playerForceMultiplier);
        }
        if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
        {
            myRBody.AddTorque(TorqueMultiplier);
        }
        if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
        {
            myRBody.AddTorque(-TorqueMultiplier);
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Shoot();
        }
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        switch (LayerMask.LayerToName(collider.gameObject.layer))
        {
            case "Asteroid":
                particles.transform.position = myTransform.position;
                particles.Play();
                eventManager.TriggerPlayerDead();
                break;

            case "Powerup":
                var powerupController = collider.gameObject.GetComponent<PowerupController>();
                StartCoroutine(UsePowerup(powerupController.Powerup));
                Destroy(collider.gameObject);
                break;
        }
    }

    // ABSTRACTION
    private IEnumerator UsePowerup(Powerup powerup)
    {
        powerup.Apply(this);
        yield return new WaitForSeconds(powerup.Duration);
        powerup.Remove(this);
    }

    // ABSTRACTION
    private void Shoot() => Instantiate(bullet, myTransform.position, myTransform.rotation);

}
